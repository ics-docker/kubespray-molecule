FROM quay.io/ansible/molecule:2.22

LABEL MAINTAINER "anders.harrisson@esss.se"

RUN adduser -D kubespray

RUN apk add --no-cache bash && \
    pip install --no-cache-dir openshift netaddr

USER kubespray
